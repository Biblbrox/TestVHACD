#include <iostream>
#include <algorithm>
#include <cassert>

#include "vhacdTools.hpp"
#include "VHACD.h"


std::pair<std::vector<float>, std::vector<uint32_t>>
vhacdtools::generateConvex(const std::vector<float>& vertices,
                           const std::vector<uint32_t>& triangles,
                           const VHACD::IVHACD::Parameters& params)
{
    VHACD::IVHACD* interfaceVHACD = VHACD::CreateVHACD();

    bool res = interfaceVHACD->Compute(&vertices[0], (unsigned int)vertices.size() / 3,
                                       (const uint32_t *)&triangles[0],
                                       triangles.size() / 3, params);

    if (!res)
        throw new std::exception("Unable to compute approximate convex\n");
   
    unsigned int nConvexHulls = interfaceVHACD->GetNConvexHulls();

    std::vector<float> res_vertices;
    std::vector<uint32_t> res_triangles;
    size_t offset = 0;
    for (unsigned int p = 0; p < nConvexHulls; ++p) {
        VHACD::IVHACD::ConvexHull ch{};
        interfaceVHACD->GetConvexHull(p, ch);

        for (size_t i = 0; i < ch.m_nPoints * 3; ++i)
            res_vertices.emplace_back(ch.m_points[i]);

        for (size_t i = 0; i < ch.m_nTriangles * 3; ++i)
            res_triangles.emplace_back(ch.m_triangles[i] + offset);

        offset += ch.m_nPoints;
    }

    //size_t max_vertex = *std::max_element(res_vertices.cbegin(), res_vertices.cend());
    //assert(max_vertex == (res_vertices.size() / 3 - 1) && "Error: maximum vertex number must be not exceed max index");

    return { res_vertices, res_triangles };
}
