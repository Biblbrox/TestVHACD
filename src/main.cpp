#include <vhacdTools.hpp>
#include <vhacdMesh.h>
#include <string>
#include <fstream>
#include <oclHelper.h>

#include "logger.hpp"


//#define CL_VERSION_1_1

#ifdef CL_VERSION_1_1
bool InitOCL(const unsigned int oclPlatformID, const unsigned int oclDeviceID, OCLHelper& oclHelper, std::ostringstream& msg);
#endif // CL_VERSION_1_1


#ifdef CL_VERSION_1_1
bool InitOCL(const unsigned int oclPlatformID, const unsigned int oclDeviceID, OCLHelper& oclHelper, std::ostringstream& msg)
{

    bool res = true;
    std::vector<std::string> info;
    res = oclHelper.GetPlatformsInfo(info, "\t\t");
    if (!res)
        return res;

    const size_t numPlatforms = info.size();
    msg << "\t Number of OpenCL platforms: " << numPlatforms << endl;
    for (size_t i = 0; i < numPlatforms; ++i) {
        msg << "\t OpenCL platform [" << i << "]" << endl;
        msg << info[i];
    }
    msg << "\t Using OpenCL platform [" << oclPlatformID << "]" << endl;
    res = oclHelper.InitPlatform(oclPlatformID);
    if (!res)
        return res;

    info.clear();
    res = oclHelper.GetDevicesInfo(info, "\t\t");
    if (!res)
        return res;

    const size_t numDevices = info.size();
    msg << "\t Number of OpenCL devices: " << numDevices << endl;
    for (size_t i = 0; i < numDevices; ++i) {
        msg << "\t OpenCL device [" << i << "]" << endl;
        msg << info[i];
    }
    msg << "\t Using OpenCL device [" << oclDeviceID << "]" << endl;
    res = oclHelper.InitDevice(oclDeviceID);
    return res;
}
#endif // CL_VERSION_1_1


bool SaveOBJ(std::ofstream& fout, const std::vector<float>& points, const std::vector<uint32_t>& triangles, size_t vertexOffset)
{
    if (fout.is_open()) {
        fout.setf(std::ios::fixed, std::ios::floatfield);
        fout.setf(std::ios::showpoint);
        fout.precision(6);

        if (points.size() > 0) {
            for (size_t v = 0; v < points.size() - 2; v += 3)
                fout << "v " << points[v + 0] << " " << points[v + 1] << " " << points[v + 2] << std::endl;   
        }
        if (triangles.size() > 0) {
            for (size_t f = 0; f < triangles.size() - 2; f += 3) {
                fout << "f "
                    << triangles[f + 0] + vertexOffset << " "
                    << triangles[f + 1] + vertexOffset << " "
                    << triangles[f + 2] + vertexOffset << " " << std::endl;
            }
        }
        return true;
    }
    else {
        return false;
    }
}


bool LoadOBJ(const std::string& fileName, std::vector<float>& points, 
             std::vector<uint32_t>& triangles)
{
    const unsigned int BufferSize = 1024;
    FILE* fid = fopen(fileName.c_str(), "r");

    if (fid) {
        char buffer[BufferSize];
        int ip[4];
        float x[3];
        char* pch;
        char* str;
        while (!feof(fid)) {
            if (!fgets(buffer, BufferSize, fid)) {
                break;
            }
            else if (buffer[0] == 'v') {
                if (buffer[1] == ' ') {
                    str = buffer + 2;
                    for (float& k : x) {
                        pch = strtok(str, " ");
                        if (pch)
                            k = (float)atof(pch);
                        else {
                            return false;
                        }
                        str = NULL;
                    }
                    points.push_back(x[0]);
                    points.push_back(x[1]);
                    points.push_back(x[2]);
                }
            }
            else if (buffer[0] == 'f') {

                pch = str = buffer + 2;
                int k = 0;
                while (pch) {
                    pch = strtok(str, " ");
                    if (pch && *pch != '\n') {
                        ip[k++] = atoi(pch) - 1;
                    }
                    else {
                        break;
                    }
                    str = NULL;
                }
                if (k == 3) {
                    triangles.push_back(ip[0]);
                    triangles.push_back(ip[1]);
                    triangles.push_back(ip[2]);
                }
                else if (k == 4) {
                    triangles.push_back(ip[0]);
                    triangles.push_back(ip[1]);
                    triangles.push_back(ip[2]);

                    triangles.push_back(ip[0]);
                    triangles.push_back(ip[2]);
                    triangles.push_back(ip[3]);
                }
            }
        }
        fclose(fid);
    }
    else {
        return false;
    }

    return true;
}

int main(int argc, char* argv[])
{

    std::vector<float> obj;
    std::vector<uint32_t> obj_triangles;
    LoadOBJ("C:\\Users\\Larin\\TestVHACD\\res\\Models\\Model.obj", obj, obj_triangles);

    VHACD::IVHACD::Parameters params;
    params.m_resolution = 46000;
    params.m_concavity = 0.0342;
    params.m_planeDownsampling = 8;
    params.m_convexhullDownsampling = 8;
    params.m_alpha = 0.0005;
    params.m_beta = 0.005;
    params.m_maxConvexHulls = 1400;
    params.m_pca = 0;
    params.m_mode = 0;
    params.m_maxNumVerticesPerCH = 17001;
    params.m_minVolumePerCH = 0.00001;
    params.m_convexhullApproximation = false;
    Logger logger;
    params.m_logger = &logger;
    auto res_model = vhacdtools::generateConvex(obj, obj_triangles, params);


    std::ofstream out_obj("out.obj");
    SaveOBJ(out_obj, res_model.first, res_model.second, 1);


    return 0;
}