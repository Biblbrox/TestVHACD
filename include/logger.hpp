#ifndef __LOGGGER__
#define __LOGGER__

class Logger : public VHACD::IVHACD::IUserLogger {
public:
    virtual ~Logger() {};
    void Log(const char* const msg) override
    {
        std::cerr << msg;
    }
};

#endif