#ifndef VHACDTOOLS_HPP
#define VHACDTOOLS_HPP

#include <vector>
#include <VHACD.h>

namespace vhacdtools
{

    /**
     * Generate approximate convex hull from vertices
     * @param vertices
     * @param resolution
     * @param concavity
     * @param planeDownSmpl
     * @param convHullDownSmpl
     * @param alpha
     * @param beta
     * @param maxConvHulls
     * @param pca
     * @param mode
     * @param maxNumVertPerCh
     * @param minVolPerCh
     * @param convHullApprox
     * @return
     */
    std::pair<std::vector<float>, std::vector<uint32_t>>
    generateConvex(const std::vector<float>& vertices,  
                   const std::vector<uint32_t>& triangles,
                   const VHACD::IVHACD::Parameters& params);    
}

#endif //VHACDTOOLS_HPP
