
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/biblbrox/sources/TestVHACD/libs/v-hacd/src/test/src/main.cpp" "/home/biblbrox/sources/TestVHACD/Release/libs/v-hacd/src/test/CMakeFiles/testVHACD.dir/src/main.cpp.o"
  "/home/biblbrox/sources/TestVHACD/libs/v-hacd/src/test/src/oclHelper.cpp" "/home/biblbrox/sources/TestVHACD/Release/libs/v-hacd/src/test/CMakeFiles/testVHACD.dir/src/oclHelper.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../VHACD_Lib/public"
  "../libs/v-hacd/src/test/inc"
  "../libs/v-hacd/src/VHACD_Lib/inc"
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/biblbrox/sources/TestVHACD/Release/libs/v-hacd/src/VHACD_Lib/CMakeFiles/vhacd.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
